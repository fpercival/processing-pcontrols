# Processing PControls

A library to easily add GUI Controls to a processing sketch.

* Automatic positioning.
* Property binding via get*Property* & set*Property*
* Colour schemes.
* Extendable.



## Getting started

1. Declare and setup PControls variable

		PControls controls;
	
		void setup()
		{
			size(800, 600, P3D);
		
			controls = new PControls(this);
		}
	
2. Create a control and attach it to a function

		controls.create("Button", "Hello").addHandler("click", "sayHello");
	This creates a button with the title "Hello". When the button is clicked, the function sayHello will we executed.
	
3. Add the handler function

		void sayHello()
		{
			System.out.println("Hi There");
		}
	When the button is clicked, the text "Hi there" will be shown in the console output.
	
## Controls

The controls available are

* Label
* Button
* Toggle
* Value
* Select
* Collection - group controls in a collapsable area.

### Label

A simple text component.

* Creating

			// Add Label with text 'Just some text'
			Label l = (Label) controls.create("Label", "Just some text");
	or
		
			controls.create("Label", "Just some text");

### Button

A simple button to handle clicks.

* Creating

			// Add Button with text 'ClickMe'
			Button b = (Button) controls.create("Button", "ClickMe");
	or
		
			controls.create("Button", "ClickMe");

* Responding to user input

			// Add Button with text 'ClickMe'
			Button b = (Button) controls.create("Button", "ClickMe");
			// Execute the function named 'buttonClicked' when the button is clicked.
			b.addHandler("click", "buttonClicked");
	or
		
			controls.create("Button", "ClickMe").addHandler("click", "buttonClicked");
	Handler example
		
			void buttonClicked(Button b)
			{
				System.out.println("Pressed button '"+b.getName()+"'");
			}


### Toggle

Like a checkbox or radio button. Best used for boolean values.

* Creating

			// Add Toggle with text 'On-Off'
			Toggle t = (Toggle) controls.create("Toggle", "On-Off");
			...
			Toggle t = (Toggle) controls.create("Toggle", "On-Off", false); // Sets the initial value to false;
	or
		
			controls.create("Toggle", "On-Off");
			...
			controls.create("Toggle", "On-Off", false); // Sets the initial value to false;

* Responding to user input

			// Add Toggle with text 'On-Off'
			Toggle t = (Toggle) controls.create("Toggle", "On-Off");
			// Execute the function named 'toggleChanged' when the toggle is clicked.
			t.addHandler("change", "toggleChanged");
	or
		
			controls.create("Toggle", "On-Off").addHandler("change", "toggleChanged");
	Handler example
		
			void toggleChanged(Toggle t)
			{
				System.out.println("Toggle changed '"+t.getName()+"'  value:"+t.getValue());
			}
			
	Alternatively, the Toggle can be directly bound to a variable.
	
			controls.create("Toggle", "On-Off").set("property", "boolVar");
	For this to work, the variable must be accessible via get & set functions using the JAVA standard. Thus..

				boolean localBoolean;
				
				void setBoolVar(boolean newValue)
				{
					localBoolean = newValue;
				}
				
				boolean getBoolVar()
				{
					return localBoolean;
				}

### Value

Control to display and change numeric values.

To change the value of this control, either.

1. Click on the control and drag the mouse up/down or left/right.
2. Hold the mouse over the control and scroll the wheel. 

Much like a traditional slider, a Value can also have a minimum and maximum value set.

* Creating

			// Add Value with text 'Steps'
			Value v = (Value) controls.create("Value", "Steps");
			...
			// Sets the initial value to 10;
			Value v = (Value) controls.create("Value", "Steps", 10); 
			...
			// Sets the initial value to 10;
			// and the range to 0..100
			Value v = (Value) controls.create("Value", "Steps", 10, 0, 100); 
	or
		
			controls.create("Value", "Steps");
			...
			// Sets the initial value to 10;
			controls.create("Value", "Steps", 10); 
			…
			// Sets the initial value to 10;
			// and the range to 0..100
			controls.create("Value", "Steps", 10, 0, 100);

* Responding to user input

			// Add Value with text 'Steps'
			Value v = (Value) controls.create("Value", "Steps");
			// Execute the function named 'valueChanged' when the value changes.
			v.addHandler("change", "valueChanged");
	or
		
			controls.create("Value", "Steps").addHandler("change", "valueChanged");
	Handler example
		
			void valueChanged(Value v)
			{
				System.out.println("Value changed '"+v.getName()+"'  value:"+v.getValue());
			}
			
	Alternatively, the Toggle can be directly bound to a variable.
	
			controls.create("Value", "Steps").set("property", "floatVar");
or

			// Set range 0..100
			controls.create("Value", "Steps", 0, 100).set("property", "floatVar");
	For this to work, the variable must be accessible via get & set functions using the JAVA standard. Thus..

				float floatVar;
				
				void setFloatVar(float newValue)
				{
					floatVar = newValue;
				}
				
				float getFloatVar()
				{
					return floatVar;
				}

### Select

Select a value from a list of options.

* Creating

			// Add Select with text 'Please choose'
			Select s = (Select) controls.create("Select", "Please choose");

* Adding options

		s.addOption("One");
		s.addOption("Two");

* Responding to user input

			// Add Select with text 'Please choose'
			Select s = (Select) controls.create("Select", "Please choose");
			// Execute the function named 'selected' when an option is selected.
			s.addHandler("change", "selected");
	or
		
			controls.create("Select", "Please choose").addHandler("change", "selected");
	Handler example
		
			void selected(Select s)
			{
				System.out.println("Select changed '"+s.getName()+"'  value:"+s.getValue());
			}
			
	Alternatively, the Toggle can be directly bound to a variable.
	
			controls.create("Select", "Please choose").set("property", "stringVar");
	For this to work, the variable must be accessible via get & set functions using the JAVA standard. Thus..

				String floatVar;
				
				void setStringVar(String newValue)
				{
					stringVar = newValue;
				}
				
				String getStringVarVar)
				{
					return stringVar;
				}

### Collection

Group controls into a collapsable area and give it a name.
Controls can be added to a collection in exactly the same way as above.

* Creating

			// Add Select with text 'Please choose'
			Collection collection = (Collection) controls.create("Collection", "My Group");

* Adding controls

		collection.create("Button", "ClickMe").addHandler("click", "buttonClicked");

	or

		collection.create("Value", "Steps").set("property", "floatVar");


## Positioning

Controls can be positioned either top, bottom, left or right of the screen.

Per default they are positioned to the left.

* To explicitly add a control to the right use.

		controls.RIGHT.create("Button", "ClickMe");
* To explicitly add a control to the top use.

		controls.TOP.create("Button", "ClickMe");
* To explicitly add a control to the bottom use.

		controls.BOTTOM.create("Button", "ClickMe");
		
* To change to default use

		controls.defaultRight();
or

		controls.defaultTop();
or

		controls.defaultBottom();


## Colour Schemes
The following colour schemes come "out of the box"

* default
* red
* blue
* green

Controls initially use the global scheme which can be changed by

		controls.setColourScheme("green");
		
However, each control can be assigned it's own colour scheme.

		controls.create("Button", "ClickMe").set("colourScheme", "green");
		
Have a look at the ColourSchemeManager Class to see how to implement a colour scheme.
		
		