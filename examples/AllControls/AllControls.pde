import net.frankpercival.processing.PControls.*;

	PControls controls;
	
	float eyeX, eyeY, eyeZ;
	float centerX, centerY, centerZ;
	float upX, upY, upZ;
	
	int ures, vres;
	
	float boxSize;
	
	float angle;
	
	boolean spin;
	boolean fill;
	
	public void setup() {
        int dwidth = 800;
        int dheight = 600;

        size(dwidth, dheight, P3D);
		
		controls = new PControls(this);
		
		eyeX = (float) (dwidth/2.0);
		eyeY = (float) (displayHeight/2.0);
		eyeZ = 0; 
		
		centerX = centerY = centerZ = 0;
		
		upX = upY = 0;
		upZ=1;
		
		ures = vres = 12;
		
		boxSize=200;
		
		angle=0;
		
		spin=false;
		
		fill=false;
		
		// Add the a value control for the box size and link it to the boxSize variable
		controls.create("Value", "Size", -500, 500).set("property", "boxSize");
		
		controls.create("Value", "URES", 3, 100).set("property", "ures");
		controls.create("Value", "VRES", 3, 100).set("property", "vres");
		
		// Create a collection for the eye position
		Collection col = (Collection)controls.create("Collection", "EYE");
		
		// Add the a value control for the eyeX to the collection and link it to the eyeX variable
		// set the "StepSize" to 1. (The number of pixels required to move the mouse to change the value)
		// set the "StepAdjust" to 1. (How much the value is changed for each step)
		// The value will change for every pixel that the mouse is moved and for each click of the mouse wheel
		col.create("Value", "X").set("property", "eyeX").set("StepSize", 1).set("StepAdjust", 10);
		// Add the a value control for the eyeY to the collection and link it to the eyeY variable
		col.create("Value", "Y").set("property", "eyeY").set("StepSize", 1).set("StepAdjust", 10);
		// Add the a value control for the eyeZ to the collection and link it to the eyeZ variable
		col.create("Value", "Z").set("property", "eyeZ").set("StepSize", 1).set("StepAdjust", 10);
		
		// Create a collection for the eye position
		col = (Collection)controls.create("Collection", "Centre");
		
		// Add the a value control for the centerX to the collection and link it to the centerX variable
		col.create("Value", "X").set("property", "centerX");
		// Add the a value control for the centerY to the collection and link it to the centerY variable
		col.create("Value", "Y").set("property", "centerY");
		// Add the a value control for the centerZ to the collection and link it to the centerZ variable
		col.create("Value", "Z").set("property", "centerZ");
		
		//Add a Toggle control to set automatic rotation and link it to the spin variable
		controls.create("Toggle", "Rotate").set("property", "spin");
		
		//Add a Toggle control to set automatic rotation and link it to the spin variable
		controls.create("Toggle", "Solid").set("property", "fill");

		//Add a Value control to adjust the z-axis rotation and link it to the angle variable
		controls.create("Value", "Angle", 0, 360).set("property", "angle");
		
		// Add a selection control to the right hand side and link it to the colourScheme property of the controls variable
		Select s = (Select)controls.RIGHT.create("Select", "Scheme").set("property", controls, "colourScheme");
		// Get a list of the colour scheme names
		String[] ss = ColourScheme.list();
		for(String sc : ss){
			// And add each one to the select
			s.addOption(sc);
		}
		// Set the colour scheme of the select control
		s.set("colourScheme", "green");
		
		//Add a spacer to the top area
		controls.TOP.create("Spacer", dwidth/3);
		//Add a label to the top of the screen
		controls.TOP.create("Label", "Processing PControls Demo");
		
		// Set the default area to the bottom area
		controls.defaultBottom();
		//Add a button and connect it to the doQuit function
		controls.create("Button", "Quit").addHandler("click", "doQuit");
	}
	
	public void doQuit() {
		this.exit();
	}
	
	public void draw() {
		background(0);
		
		if(fill) fill(200);
		else noFill();

		camera(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ);
		stroke(255);
		rotateZ(radians(angle));
		if(spin) {
			if(angle++>=360) angle=1;
		}

//		box(boxSize);
		sphereDetail(ures, vres);
		sphere(boxSize);
	}
	
	
	public float getEyeX() {
		return eyeX;
	}

	public void setEyeX(float eyeX) {
		this.eyeX = eyeX;
	}

	public float getEyeY() {
		return eyeY;
	}

	public void setEyeY(float eyeY) {
		this.eyeY = eyeY;
	}

	public float getEyeZ() {
		return eyeZ;
	}

	public void setEyeZ(float eyeZ) {
		this.eyeZ = eyeZ;
	}

	public float getCenterX() {
		return centerX;
	}

	public void setCenterX(float centerX) {
		this.centerX = centerX;
	}

	public float getCenterY() {
		return centerY;
	}

	public void setCenterY(float centerY) {
		this.centerY = centerY;
	}

	public float getCenterZ() {
		return centerZ;
	}

	public void setCenterZ(float centerZ) {
		this.centerZ = centerZ;
	}

	public float getBoxSize() {
		return boxSize;
	}

	public void setBoxSize(float boxSize) {
		this.boxSize = boxSize;
	}

	public float getAngle() {
		return angle;
	}

	public void setAngle(float angle) {
		this.angle = angle;
	}

	public boolean getSpin() {
		return spin;
	}

	public void setSpin(boolean spin) {
		this.spin = spin;
	}

	public float getUres() {
		return ures;
	}

	public void setUres(float ures) {
		this.ures = (int)ures;
	}

	public float getVres() {
		return vres;
	}

	public void setVres(float vres) {
		this.vres = (int)vres;
	}

	public boolean getFill() {
		return fill;
	}

	public void setFill(boolean fill) {
		this.fill = fill;
	}
