package net.frankpercival.processing.PControls;
import processing.core.PApplet;
import processing.event.KeyEvent;
import processing.event.MouseEvent;


public class PControls {
	
	public static String VERSION = "1.0.0";
	
	PApplet p;
	
	public static ControlGroup TOP;
	public static ControlGroup BOTTOM;
	public static ControlGroup LEFT;
	public static ControlGroup RIGHT;

	private ControlGroup dflt;
	
	boolean rebuild;
	
	ColourScheme scheme;

	public PControls(PApplet parent) {
		System.err.println("PControls v"+VERSION);
		p = parent;
		init();
	}
	
	protected void init() 
	{
		TOP = new ControlGroup(p, ControlGroup.TOP, this);
		TOP.scheme = null;
		BOTTOM = new ControlGroup(p, ControlGroup.BOTTOM, this);
		BOTTOM.scheme = null;
		LEFT = new ControlGroup(p, ControlGroup.LEFT, this);
		LEFT.scheme = null;
		RIGHT = new ControlGroup(p, ControlGroup.RIGHT, this);
		RIGHT.scheme = null;
		defaultLeft();
		p.registerMethod("draw", this);
		rebuild = true;
		scheme = ColourSchemeManager.MANAGER().getScheme("default");
	}
	
	public void testfill(int nm) {
//		TOP.testfill("TOP", nm);
		BOTTOM.testfill("BOTTOM", nm);
		LEFT.testfill("LEFT", nm);
		RIGHT.testfill("RIGHT", nm);
	}
	
	protected void layout()
	{
		if(!rebuild) return;
		TOP.calcrect();
		BOTTOM.calcrect();
		LEFT.miny = TOP.height;
		LEFT.maxv = p.height - (TOP.height+BOTTOM.height);
		RIGHT.miny = LEFT.miny;
		RIGHT.maxv = LEFT.maxv;
		LEFT.calcrect();
		RIGHT.calcrect();
		rebuild = false;
	}
	
	public void refresh()
	{
		rebuild = true;
	}
	
	public void draw()
	{
		layout();
		TOP.prepdraw();
		drawGroup(TOP);
		drawGroup(BOTTOM);
		drawGroup(LEFT);
		drawGroup(RIGHT);
		TOP.finishdraw();
	}
	
	protected void drawGroup(ControlGroup grp)
	{
		if(grp.active) grp.drawControls();
	}
	
	protected boolean eventInGroup(ControlGroup p, MouseEvent e){
		int mx = e.getX();
		int my = e.getY();
		
		if(mx>=p.x1 && mx<=p.x2 && my>=p.y1 && my<=p.y2) return true;
		return false;
	}
	
	public void defaultTop(){ dflt = TOP; }
	public void defaultBottom(){ dflt = BOTTOM; }
	public void defaultLeft(){ dflt = LEFT; }
	public void defaultRight(){ dflt = RIGHT; }

	public void addControl(Control c) {
		if(c==null) return;
		dflt.addControl(c);
	}

	public PControls onMouseEnter(String function, Object object) {
		TOP.onMouseEnter(function, object);
		BOTTOM.onMouseEnter(function, object);
		LEFT.onMouseEnter(function, object);
		RIGHT.onMouseEnter(function, object);
		return this;
	}

	public PControls onMouseExit(String function, Object object) {
		TOP.onMouseExit(function, object);
		BOTTOM.onMouseExit(function, object);
		LEFT.onMouseExit(function, object);
		RIGHT.onMouseExit(function, object);
		return this;
	}

	public Control create(String name, Object... objs) {
		return dflt.create(name, objs);
	}
	
	public PControls setColourScheme(String name) {
		ColourScheme s = ColourScheme.get(name);
		if(s!=null) {
			scheme = s;
		}
		return this;
	}

	public String getColourScheme() {
		return scheme.name;
	}

}
