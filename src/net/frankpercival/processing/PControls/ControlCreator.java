package net.frankpercival.processing.PControls;
import java.lang.reflect.Constructor;
import java.util.Hashtable;


public class ControlCreator {
	
	ControlGroup g;
	
	Hashtable<String, Class> cls;

	public ControlCreator(ControlGroup group) {
		g = group;
		cls = new Hashtable();
	}
	
	public Control create(String name, Object ...objs)
	{
		Control c=null;
		try
		{
			Class clz = findClass(name);
			if(clz==null) return null;
			
			Class[] cs = new Class[objs.length+1];
			Object[] os = new Object[objs.length+1];
			cs[0] = g.getClass();
			os[0] = g;
			for(int i=0; i<objs.length; i++)
			{
				cs[i+1] = objs[i].getClass();
				os[i+1] = objs[i];
			}
			Constructor<Control> con = null; // = clz.getConstructor(cs);
			con = Helper.findConstructor(clz, cs);
			if(con!=null) {
/*
				Class[] ps = con.getParameterTypes();
				for(int i=0; i<ps.length; i++) {
					try{
						os[i] = ps[i].cast(os[i]);
					} catch(Exception ee){
						throw(ee);
					}
				}
*/
				c = con.newInstance(os);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		if(c==null){
			System.err.println("NoSuchControl ");
		}
		return c;
	}
	
	protected Class findClass(String name)
	{
		String n = name; //name.toLowerCase();
		Class c = cls.get(n);
		if(c!=null) return c;
		try
		{
			String fn = name;
			try {
				c = getClass().forName(fn);
			} catch(Exception e1){
				c = null;
			}
			if(c==null) {
				if(Control.class.getPackage()!=null)
				{
					fn = Control.class.getPackage().getName()+"."+fn;
				}
				c = getClass().forName(fn);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		if(c!=null) {
			cls.put(n, c);
		}
		return c;
	}
	
	public void addDefinition(String name, Class clz) {
		if(clz!=null && name!=null && name.length()>0) {
			cls.put(name, clz);
		}
	}

}
