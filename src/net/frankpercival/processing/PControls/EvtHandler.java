package net.frankpercival.processing.PControls;
import java.lang.reflect.Method;
import java.util.Hashtable;


public class EvtHandler {
	Hashtable<String, Method> meths;
	Hashtable<String, Object> methObjs;

	public EvtHandler() {
		meths = new Hashtable();
		methObjs = new Hashtable();
	}
	
	public Method addHandler(String handlername, Object object, String fname, Class[] args) {
		Method m=null;
		try
		{
			m = object.getClass().getDeclaredMethod(fname, args);
		} catch(Exception e) {
			m= Helper.findMethod(object, fname, args, true);
			if(m==null)
			{
				e.printStackTrace();
			}
		}
		
		if(m!=null)
		{
			meths.put(handlername, m);
			methObjs.put(handlername, object);
		}
		return m;
	}
	
	public Object fireHanlder(String handlername, Object[] args) {
		Object rt=null;
		Method m = meths.get(handlername);
		if(m!=null) {
			try {
				Object to = methObjs.get(handlername);
				if(m.getParameterTypes().length==0) rt = m.invoke(to, null);
				else rt = m.invoke(to, args);
			} catch(Exception e) {}
		}
		return rt;
	}

}
