package net.frankpercival.processing.PControls;
import java.util.Vector;

import processing.core.PApplet;
import processing.event.MouseEvent;


public class Collection extends Control {
	
	public static final int HORIZONTAL = 1;
	public static final int VERTICAL = 2;
	
	Vector<Control> cs;
	int layout;
	
	boolean open;
	
	Control mfcs;
	
	ArrowToggle tg;
	
	private ControlCreator creator;

	public Collection(ControlGroup group, String name) {
		super(group);
		cs = new Vector();
		this.name = name;
/*
		if(g.position == g.RIGHT || g.position == g.LEFT) {
			layout = VERTICAL;
		} else {
			layout = HORIZONTAL;
		}
*/
		layout = VERTICAL;
		open=true;
		border = false;
		
		creator = new ControlCreator(group); 
		
		tg = (ArrowToggle) new ArrowToggle(g, name, open).set("property", this, "open");
//		tg.addHandler("click", "toggle", this); //.setHeight(10);
		addControl(tg);
		
		close();
	}
	
	void handleMouseEvent(MouseEvent e)
	{
		for(int i=0; i<cs.size(); i++) {
			Control c = cs.get(i);
			if(c.visible && c.eventInControl(e)) {
				if(mfcs!=c)
				{
					if(mfcs!=null) {
						mfcs.handleMouseExit(e);
					}
					c.handleMouseEnter(e);
				}
				mfcs = c;
				c.handleMouseEvent(e);
				return;
			}
		}
		if(mfcs!=null) {
			mfcs.handleMouseExit(e);
		}
		mfcs=null;
	}
	
	public void handleMouseEnter(MouseEvent e){
	}
	
	public void handleMouseExit(MouseEvent e){
		if(mfcs!=null) {
			mfcs.handleMouseExit(e);
		}
		mfcs=null;
	}
	
	public void toggle(Toggle t) {
//System.out.println("Collection '"+getName()+"' :"+open);
		open = tg.value();
		if(open==true) open();
		else close();
		layout();
	}
	
	protected void open()
	{
		open=true;
		tg.setValue(open);
		for(int i=1; i<cs.size(); i++) {
			cs.get(i).show();
		}
		layout();
	}
	
	protected void close()
	{
		open=false;
		tg.setValue(open);
		for(int i=1; i<cs.size(); i++) {
			cs.get(i).hide();
		}
		layout();
	}
	
	protected void sizeChanged(){
		layout();
	}
	
	protected void layout(){
		layout(true);
	}
	
	protected void layout(boolean resize) {
		float w=0, h=0;
		Control c;
		for(int i=0; i<cs.size(); i++) {
			c = cs.elementAt(i);
			if(c.visible) 
			{
				if(i>0) {
					if(layout==VERTICAL) {
						h += g.pad;
					} else {
						w += g.pad;
					}
				}
				if(layout==VERTICAL) {
					c.x = x + g.pad;
					c.y = y+h;
					w = Math.max(w, c.getWidth() + g.pad);
					h += c.getHeight();
				} else {
					c.x = x+w;
					c.y = y + g.pad;
					w += c.getWidth();
					h = Math.max(h, c.getHeight() + g.pad);
				}
				if(i==0) {
					if(layout==VERTICAL) {
						c.x -= g.pad;
					} else {
						c.y -= g.pad;
					}
				}
				c.recalc();
			}
		}
		if(layout==VERTICAL) {
			if(w>0) w += g.pad;
		} else {
			if(h>0) h += g.pad;
		}
		if(resize) setDimensions(w, h);
//System.out.println("Collection '"+getName()+"'  size:"+cs.size()+"   height:"+height+"   d:"+w+"x"+h+"   p:"+x+"x"+y);
	}
	
	protected void recalc(){
		layout(false);
	}

	
	void render(PApplet p)
	{
		super.render(p);
		for(int i=0; i<cs.size(); i++){
			Control c = cs.elementAt(i);
			if(c.visible) c.render(p);
		}
	}

	@Override
	public void draw(PApplet p) {
		for(int i=0; i<cs.size(); i++){
			Control c = cs.elementAt(i);
			if(c.visible) c.draw(p);
		}
	}

	@Override
	public void draw2(PApplet p) {
		for(int i=0; i<cs.size(); i++){
			Control c = cs.elementAt(i);
			if(c.visible) c.draw2(p);
		}
	}
	
	protected void addControl(Control c) {
		if(c==null) return;
		cs.add(c);
		c.parent = this;
		c.visible = open;
		layout();
	}
	
	public Collection addCollection(String name)
	{
		Collection c = new Collection(this.g, name);
		addControl(c);
		return c;
	}

	public Control create(String name, Object... objs) {
		Control c = creator.create(name, objs);
		addControl(c);
		return c;
	}

	public boolean getOpen() {
		return open;
	}

	public void setOpen(boolean o) {
		if(o==open) return;
		if(o) open();
		else close();
	}

	@Override
	public float getHeight() {
		if(g.position==g.TOP ) {
			return cs.elementAt(0).getHeight();
		}
		return super.getHeight();
	}
}
