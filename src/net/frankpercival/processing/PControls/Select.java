package net.frankpercival.processing.PControls;

import processing.core.PApplet;

public class Select extends Collection {
	
	boolean multiple;
	
	String val;
	
	int optionHeight;
	
	String delimiter;
	
	PropertyConnector<String> link;

	public Select(ControlGroup group, String name) {
		super(group, name);
		multiple = false;
		val = "";
		optionHeight = 10;
		delimiter=",";
		link = new PropertyConnector("");
		link.setDest(this, "pval");
	}
	
	public Select setProperty(Object object, String property) {
		link.setProperty(object, property);
		return this;
	}
	
	public Select setProperty(String property) {
		link.setProperty(g.p, property);
		return this;
	}
	
	public void drawPrep(PApplet p){
		link.pull();
	}
	
	public Select addOption(String value){
		Toggle t = new Toggle(g, value, cs.size()==1);
		t.addHandler("click", "optionClick", this);
		t.border = false;
		t.height = optionHeight;
		addControl(t);
		checkval();
		return this;
	}
	
	public void optionClick(Toggle t){
		String v = t.name;
		
		if(multiple==false && v.equals(val))
		{
			if(t.value()==false)  t.setValue(true);
			return;
		}
		
		Toggle tt;
		for(int i=1; i<cs.size(); i++){
			tt = (Toggle) cs.get(i);
			if(multiple==false) {
				if(!tt.name.equals(v)) {
					tt.setValue(false);
				}
			}
		}
		v = getVal();
		if(!v.equals(val)) {
			val = v;
			link.push();
			fireHandler("change");
		}
	}
	
	protected void checkval() {
		String s = delimiter+val+delimiter;
		for(int i=1; i<cs.size(); i++) {
			Toggle t = (Toggle) cs.elementAt(i);
			if(s.indexOf(delimiter+t.name+delimiter)>=0) {
				t.setValue(true);
			} else {
				t.setValue(false);
			}
		}
	}
	
	protected String getVal() {
		String v = "";
		Toggle t;
		for(int i=1; i<cs.size(); i++){
			t = (Toggle)cs.elementAt(i);
			if(t.value) {
				if(multiple==false) return t.name;
				if(v.length()>0) v += delimiter;
				v += t.name;
			}
		}
		return v;
	}

	public boolean isMultiple() {
		return multiple;
	}

	public boolean getMultiple() {
		return multiple;
	}

	public void setMultiple(boolean multiple) {
		this.multiple = multiple;
	}

	public String getValue() {
		link.pull();
		return val;
	}
	
	public Select setValue(String nval){
		val = nval;
		checkval();
		link.push();
		return this;
	}

	public String getPval() {
		return val;
	}
	
	public void setPval(String nval){
		val = nval;
		checkval();
	}

	public String getDelimiter() {
		return delimiter;
	}

	public Select setDelimiter(String delimiter) {
		this.delimiter = delimiter;
		return this;
	}

}
