package net.frankpercival.processing.PControls;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;


public class Helper {

	public Helper() {
	}
	
	public static Method findMethod(Object object, String fname, Object[] args)
	{
		return findMethod(object, fname, args, false);
	}
	public static Method findMethod(Object object, String fname, Object[] args, boolean allowEmpty)
	{
		Class[] cs = new Class[args.length];
		for(int i=0; i<args.length; i++)
		{
			cs[i] = args[i].getClass();
		}
		return findMethod(object, fname, cs);
	}
	
	public static Method findMethod(Object object, String fname, Class[] args)
	{
		return findMethod(object, fname, args, false);
	}
	
	public static Method findMethod(Object object, String fname, Class[] args, boolean allowEmpty)
	{
		Method m =null;
		try
		{
			Method[] ms = object.getClass().getMethods();
			for(int i=0; i<ms.length && m==null; i++)
			{
				m = ms[i];
				Class[] cls = m.getParameterTypes();
				Class dstc;
				if(cls.length==args.length && m.getName().equalsIgnoreCase(fname))
				{
					boolean isok=true;
					for(int j=0; j<args.length && isok; j++)
					{
						dstc = args[j];
//						isok = args[j].isAssignableFrom(cls[j]);
						isok = cls[j].isAssignableFrom(dstc);
						if(!isok) {
							if(dstc == Integer.class) dstc = Integer.TYPE;
							if(dstc == Float.class) dstc = Float.TYPE;
							if(dstc == Double.class) dstc = Double.TYPE;
							if(dstc == Boolean.class) dstc = Boolean.TYPE;

							isok = cls[j].isAssignableFrom(dstc);
						}
					}
					if(isok==false)
					{
						m=null;
					}
				} else {
					m=null;
				}
			}
			if(m==null && allowEmpty) {
				try
				{
					m = object.getClass().getMethod(fname, null);
				} catch(Exception ee) {}
			}
		} catch(Exception ex) {
			ex.printStackTrace();
			m=null;
		}
		return m;
	}
	
	public static Constructor findConstructor(Object object, Object[] args){
		Class[] cs = new Class[args.length];
		for(int i=0; i<args.length; i++)
		{
			cs[i] = args[i].getClass();
		}
		return findConstructor(object.getClass(), cs);
	}
	
	public static Constructor findConstructor(Class clz, Class[] args)
	{
		Constructor con =null;
		try
		{
			Constructor[] cons = clz.getConstructors();
			for(int j=0; j<cons.length && con==null; j++)
			{
				boolean isok=true;
				con = cons[j];
				Class[] parms = con.getParameterTypes();
				if(parms.length == args.length)
				{
					Class dstc;
					for(int k=0; k<args.length && isok; k++)
					{
						dstc = args[k];
						isok = parms[k].isAssignableFrom(dstc);
						if(!isok) {
							if(dstc == Integer.class) dstc = Integer.TYPE;
							if(dstc == Float.class) dstc = Float.TYPE;
							if(dstc == Double.class) dstc = Double.TYPE;
							if(dstc == Boolean.class) dstc = Boolean.TYPE;

							isok = parms[k].isAssignableFrom(dstc);
//							if(!isok) isok = dstc.isAssignableFrom(parms[k]);
						}
						if(!isok) {
							try{
								parms[k].asSubclass(args[k]);
								isok=true;
							} catch(Exception ee) {
								//ee.printStackTrace();
								isok=false;
							}
						}
					}
					if(!isok) con=null;
				} else {
					con=null;
				}
			}
		} catch(Exception ex) {
			ex.printStackTrace();
			con=null;
		}
		return con;
	}

}
