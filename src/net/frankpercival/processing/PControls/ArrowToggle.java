package net.frankpercival.processing.PControls;
import processing.core.PApplet;


public class ArrowToggle extends Toggle {

	public ArrowToggle(ControlGroup group, String name, boolean val) {
		super(group, name, val);
		border = false;
		changeOnHover = true;
		changeOnMouseDown = true;
	}

	@Override
	public void draw(PApplet p) {
		float r = p.getFont().getSize2D()/2; //height - 10;
		
		float pd = (height-r)/2;
		float tx = border?x+pd+r:x+r;
		float ty = y+(height/2);
		
		int col = csi.fillCol();
		p.fill(col);
		p.stroke(col);
		p.beginShape();
		if(value)
		{
			p.vertex(tx-r, ty-r);
			p.vertex(tx+r, ty-r);
			p.vertex(tx, ty+r);
		} else {
			p.vertex(tx-r, ty+r);
			p.vertex(tx-r, ty-r);
			p.vertex(tx+r, ty);
		}
		p.endShape(p.CLOSE);

		tx = tx+r+pd;
		ty = y+(height/2);
		p.textAlign(p.LEFT, p.CENTER);
		drawText(name, tx, ty);
	}

}
