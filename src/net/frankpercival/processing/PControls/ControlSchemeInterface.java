package net.frankpercival.processing.PControls;

public class ControlSchemeInterface {
	
	Control c;
	boolean hover=false;
	boolean mdwn=false;

	public ControlSchemeInterface(Control control) {
		c = control;
	}
	
	protected ColourScheme s() {
		return c.colourScheme();
	}
	
	public boolean ishover() { return hover;}
	public void hover() { hover=true;}
	public void hover(boolean ishover) { hover=ishover;}
	public void unhover() { hover=false;}
	
	public boolean isMouseDown() { return mdwn;}
	public void setmousedown() { mdwn=true;}
	public void setmousedown(boolean isdown) { mdwn=isdown;}
	public void setmouseup() { mdwn=false;}
	
	public int textCol()
	{
		if(mdwn) return s().MDWN_TEXT_COLOUR;
		return hover?s().HOVER_TEXT_COLOUR:s().TEXT_COLOUR;
	}
	
	public int lineCol()
	{
		if(mdwn) return s().MDWN_LINE_COLOUR;
		return hover?s().HOVER_LINE_COLOUR:s().LINE_COLOUR;
	}
	
	public int fillCol()
	{
		if(mdwn) return s().MDWN_FILL_COLOUR;
		return hover?s().HOVER_FILL_COLOUR:s().FILL_COLOUR;
	}
	
	public int backCol()
	{
		if(mdwn) return s().MDWN_BACKGROUND_COLOUR;
		return hover?s().HOVER_BACKGROUND_COLOUR:s().BACKGROUND_COLOUR;
	}
	
	public void drawText(String txt, float x, float y) {
		c.g.p.fill( textCol() );
		c.g.p.text(txt, x, y);
	}
	
	public void drawText(String txt, float x, float y, float w, float h) {
		c.g.p.fill( textCol() );
		c.g.p.text(txt, x, y, w, h);
	}
	
	public void drawRect(float x, float y, float width, float height, boolean fill) {
		ColourScheme s = s();
		c.g.p.stroke( lineCol() );
		if(fill) c.g.p.fill( fillCol() );
		else c.g.p.noFill();
		c.g.p.rect(x, y, width, height, s.CORNER_RADIUS);
	}
	
	public void drawRect(float x, float y, float width, float height)
	{
		drawRect(x, y, width, height, s().FILL_AS_DEFAULT);
	}
	
	public void drawBorder()
	{
		c.g.p.stroke( lineCol() );
		c.g.p.fill( backCol() );
		c.g.p.rect(c.x, c.y, c.width, c.height, s().CORNER_RADIUS);
	}
	
	public void drawEllipse(float x, float y, float width, float height, boolean fill) {
		ColourScheme s = s();
		c.g.p.stroke( lineCol() );
		if(fill) c.g.p.fill( fillCol() );
		else c.g.p.noFill();
		c.g.p.ellipse(x, y, width, height);
	}
	
	public void drawEllipse(float x, float y, float width, float height)
	{
		drawEllipse(x, y, width, height, s().FILL_AS_DEFAULT);
	}

}
