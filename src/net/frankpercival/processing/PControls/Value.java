package net.frankpercival.processing.PControls;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import processing.core.PApplet;
import processing.event.MouseEvent;


public class Value extends Control {
	
	float min;
	float max;
	float value, oval;
	
	int stepSize;
	float stepAdjust;
	
	int sx, sy;

	DecimalFormat nf;
	
	private PropertyConnector<Float> link;

	public Value(ControlGroup group, String nme, float val, float minimum, float maximum) {
		super(group);
		name = nme;
		min = minimum;
		max = maximum;
		value = val;
		stepAdjust = 1;
		stepSize = 10;
		nf= new DecimalFormat("#0.00");
		init();
	}
	
	public Value(ControlGroup group, String nme, Object val, Object minimum, Object maximum){
		this(group, nme, Float.parseFloat(""+val), Float.parseFloat(""+minimum), Float.parseFloat(""+maximum));
	}

	public Value(ControlGroup group, String nme, Object minimum, Object maximum){
		this(group, nme, 0, Float.parseFloat(""+minimum), Float.parseFloat(""+maximum));
	}
	
	public Value(ControlGroup group, String nme, Object val) {
		this(group, nme, Float.parseFloat(""+val), 0, 0);
	}
	
	public Value(ControlGroup group, String nme) {
		this(group, nme, 0, 0, 0);
	}

	public void init() {
		float mx = max;
		if(mx==0) mx=100;
		float nw = g.p.textWidth( getStr(mx))+20;
//System.out.println(getName()+" - nw:"+nw);
		if(nw>width) setWidth(nw);
		link = new PropertyConnector(0f);
		link.setDest(this, "pval");
	}
	
	public Value setProperty(Object object, String property) {
		link.setProperty(object, property);
		return this;
	}
	
	public Value setProperty(String property) {
		link.setProperty(g.p, property);
		return this;
	}
	
/*
	protected float getLinkedValue(){
		float f=0;
		if(link.isSet()) {
			f = link.get();
			value = f;
		}
		return f;
	}
	
	protected Value setLinkedValue(){
		if(link.isSet()) link.set(value);
		return this;
	}
*/
	
	public Value setChangeHandler(String function, Object object) {
		addHandler("change", object, function, new Class[]{this.getClass()});
		return this;
	}
	
	public void mouseEvent(MouseEvent e){
		int mx=e.getX();
		int my=e.getY();

		if(e.getAction()==e.PRESS) {
			lockmouse();
			sx=mx;
			sy=my;
			oval = value;
		}
		else if(e.getAction()==e.RELEASE) {
			freemouse();
			if(oval!=value) {
				changeValue();
			}
		}
		else if(e.getAction()==e.DRAG) {
			if(this.lckd) {
				int dx=mx-sx, dy=sy-my;
				int d = dx+dy;
				updval( oval + getAdj(d) );
			}
		}
		else if(e.getAction()==e.WHEEL) {
			if(e.getCount()!=0) {
				updval( value - getAdj(e.getCount()*stepSize), true );
			}
		}
	};
	
	public void mouseEnter(MouseEvent e) {
		hover();
	}
	
	public void mouseExit(MouseEvent e) {
//		freemouse();
		unhover();
	}
	
	protected float getAdj(int df) {
		return Math.round(df/stepSize)*stepAdjust;
	}
	
	protected void updval(float nval) {
		updval(nval, false);
	}
	
	protected void updval(float nval, boolean fire) {
		float oval=value;
		float cval = nval;
		if(min!=max) {
			cval = Math.max(cval, min);
			cval = Math.min(cval, max);
		}
		value = cval;
		if(fire==true && oval!=value) changeValue();
	}
	
	protected void changeValue() {
//		fireHanlder("change", new Object[]{this});
//		setLinkedValue();
		link.push();
		fireHandler("change");
	}

	@Override
	public void draw(PApplet p) {
		if(!lckd) link.pull(); //getLinkedValue();
		p.textAlign(p.LEFT, p.CENTER);
		float tx = x+10;
		float ty = y+(height/2);
		drawText(getStr(value), tx, ty);
	}
	
	protected String getStr(float v) {
		return name+": "+nf.format(v);
	}

	public float getMin() {
		return min;
	}

	public Value setMin(float min) {
		this.min = min;
		return this;
	}

	public float getMax() {
		return max;
	}

	public Value setMax(float max) {
		this.max = max;
		return this;
	}

	public float getValue() {
		link.pull();
		return value;
	}

	public Value setValue(float value) {
		this.value = value;
		link.push();
		return this;
	}

	public float getPval() {
		return value;
	}

	public void setPval(float value) {
		this.value = value;
	}

	public int getStepSize() {
		return stepSize;
	}

	public Value setStepSize(int stepSize) {
		this.stepSize = stepSize;
		return this;
	}

	public float getStepAdjust() {
		return stepAdjust;
	}

	public Value setStepAdjust(float stepAdjust) {
		this.stepAdjust = stepAdjust;
		return this;
	}

	public Value setStepAdjust(int stepAdjust) {
		return setStepAdjust((float)stepAdjust);
	}
}
