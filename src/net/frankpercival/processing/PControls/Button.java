package net.frankpercival.processing.PControls;
import processing.core.PApplet;
import processing.event.MouseEvent;


public class Button extends Control {

	public Button(ControlGroup group, String label) {
		super(group);
		name = label;
	}
	
	public Button setClickHandler(String function, Object object) {
		addHandler("click", object, function, new Class[]{this.getClass()});
System.out.println("Added click handler: "+getName());
		return this;
	}
	
	protected void click() {
//		fireHanlder("click", new Object[]{this});
	}
	
	@Override
	public void mouseClick(MouseEvent e){
		click();
	};

	@Override
	public void draw(PApplet p) {
		p.textAlign(p.CENTER, p.CENTER);
		float tx = x+(width/2);
		float ty = y+(height/2);
		this.drawText(name, tx, ty);
	}
	
	

}
