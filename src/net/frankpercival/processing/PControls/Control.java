package net.frankpercival.processing.PControls;
import java.lang.reflect.Method;
import java.util.Hashtable;

import processing.core.*;
import processing.event.KeyEvent;
import processing.event.MouseEvent;


public abstract class Control extends FieldSetter {
	static int nid=1;
	
	float width;
	float height;
	float x;
	float y;
	int rid;
	ControlGroup g;
	boolean lckd;
	
	boolean visible;
	
	EvtHandler eh;
	
	ColourScheme scheme;
	ControlSchemeInterface csi;
	
	Control parent;
	
	String name;
	private int id;
	
	boolean border;
	boolean changeOnHover;
	boolean changeOnMouseDown;
	
	int mousex, mousey, lastmousex, lastmousey;
	
	public Control(ControlGroup group)
	{
		g = group;
		lckd = false;
		eh = new EvtHandler();
		width = g.defaultWidth;
		height = g.defaultHeight;
		visible = true;
		id = nid++;
		csi = new ControlSchemeInterface(this);
		border = true;
		changeOnMouseDown = true;
		changeOnHover = true;
	}
	
	void render(PApplet p)
	{
		if(border==true)
		{
			p.rectMode(p.CORNER);
			csi.drawBorder();
		}
		drawPrep(p);
		draw(p);
	}
	
	protected void sizeChanged(){}
	
	protected void refresh() {
		if(parent!=null) parent.sizeChanged();
		g.refresh();
	}
	
	protected void recalc(){}

	public void drawPrep(PApplet p){}
	public abstract void draw(PApplet p);
	public void draw2(PApplet p){};
	
	protected void lockmouse() {
		lckd = g.lockmouse(this);
	}
	
	protected void freemouse() {
		if(!lckd) return;
		lckd=false;
		g.unlockmouse();
	}
	
	public boolean eventInControl(MouseEvent e){
		int mx = e.getX();
		int my = e.getY();
		
		if(mx>=x && mx<=(x+width) && my>=y && my<=(y+height)) return true;
		return false;
	}
	
	public void handleMouseEnter(MouseEvent e){
		if(changeOnHover) hover();
		mouseEnter(e);
	}
	
	public void handleMouseExit(MouseEvent e){
		if(changeOnHover) unhover();
		mouseExit(e);
	}
	
	public void mouseEnter(MouseEvent e){
	}
	
	public void mouseExit(MouseEvent e){
	}

	void handleMouseEvent(MouseEvent e) {
		lastmousex = mousex;
		lastmousey = mousey;
		mousex = e.getX();
		mousey = e.getY();
		mouseEvent(e);
		switch (e.getAction()) {
			case MouseEvent.WHEEL:
				mouseWheel(e);
				fireHandler("wheel");
				break;
			case MouseEvent.CLICK:
				if(e.getCount()>1) {
					mouseDoubleClick(e);
					fireHandler("doubleClick");
				} else {
					mouseClick(e);
					fireHandler("click");
				}
				break;
			case MouseEvent.DRAG:
				mouseDrag(e);
				fireHandler("drag");
				break; 
			case MouseEvent.PRESS:
				if(changeOnMouseDown) setmousedown();
				mousePress(e);
				fireHandler("mousePress");
				break; 
			case MouseEvent.RELEASE:
				if(changeOnMouseDown) setmouseup();
				mouseRelease(e);
				fireHandler("mouseRelease");
				break; 
		}
	}
	
	public void mouseEvent(MouseEvent e){}
	
	protected void mouseClick(MouseEvent e){}
	protected void mouseDoubleClick(MouseEvent e){}
	protected void mouseWheel(MouseEvent e){}
	protected void mouseDrag(MouseEvent e){}
	protected void mousePress(MouseEvent e){}
	protected void mouseRelease(MouseEvent e){}
	
	public boolean keyEvent(KeyEvent e){
		return false;
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
		refresh();
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
		refresh();
	}

	public void setDimensions(float w, float h) {
		this.width = w;
		this.height = h;
		refresh();
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}
	
	public Control addHandler(String handlername, String fname)
	{
		return addHandler(handlername, fname, g.p);
	}

	public Control addHandler(String handlername, String fname, Object object) {
		eh.addHandler(handlername, object, fname, new Class[]{this.getClass()});
		return this;
	}

	protected Method addHandler(String handlername, Object object, String fname,
			Class[] args) {
		return eh.addHandler(handlername, object, fname, args);
	}

	protected void fireHandler(String handlername) {
		eh.fireHanlder(handlername, new Object[]{this});
	}

	protected Object fireHanlder(String handlername, Object[] args) {
		return eh.fireHanlder(handlername, args);
	}
	
	public Control set(String fld, Object ...objs)
	{
		super.set(fld, objs);
		return this;
	}
	
	public void show(){
		visible = true;
	}
	
	public void hide(){
		visible = false;
	}
	
	public String getName()
	{
		String rt = getSimpleName();
		if(parent!=null && parent instanceof Collection) rt = parent.getName()+"."+rt;
		return rt;
	}
	
	public String getSimpleName()
	{
		String rt = name;
		if(rt==null) rt = getClass().getSimpleName() + "_"+id;
		return rt;
	}
	
	public ColourScheme colourScheme() {
		if(scheme==null) {
			if(parent!=null) return parent.colourScheme();
			return g.scheme();
		}
		return scheme;
	}
	
	public Control setColourScheme(String sname) {
		ColourScheme s = ColourSchemeManager.MANAGER().getScheme(sname);
		if(s==null && scheme!=null) {
			System.err.println("** Warning: colour scheme removed will use scheme in controls '"+g.scheme.name+"'");
		}
		scheme = s;
		return this;
	}
	
	public String getColourScheme() {
		return colourScheme().name;
	}
	
	public String toString()
	{
		return getName();
	}

	public void drawText(String txt, float x, float y) {
		csi.drawText(txt, x, y);
	}

	public void drawRect(float x, float y, float width, float height,
			boolean fill) {
		csi.drawRect(x, y, width, height, fill);
	}

	public void drawRect(float x, float y, float width, float height) {
		csi.drawRect(x, y, width, height);
	}

	public void drawEllipse(float x, float y, float width, float height,
			boolean fill) {
		csi.drawEllipse(x, y, width, height, fill);
	}

	public void drawEllipse(float x, float y, float width, float height) {
		csi.drawEllipse(x, y, width, height);
	}

	public boolean ishover() {
		return csi.ishover();
	}

	public void hover() {
		csi.hover();
	}

	public void hover(boolean ishover) {
		csi.hover(ishover);
	}

	public void unhover() {
		csi.unhover();
	}

	public void setmousedown() {
		csi.setmousedown();
	}

	public void setmousedown(boolean isdown) {
		csi.setmousedown(isdown);
	}

	public void setmouseup() {
		csi.setmouseup();
	}

	public Control setName(String name) {
		this.name = name;
		return this;
	}

	public boolean getBorder() {
		return border;
	}

	public void setBorder(boolean border) {
		this.border = border;
	}

}
