package net.frankpercival.processing.PControls;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;


public class ColourSchemeManager {
	
	private static ColourSchemeManager sm;
	
	public static ColourSchemeManager MANAGER(){
		if(sm==null) sm = new ColourSchemeManager();
		return sm;
	}
	
	Hashtable<String, ColourScheme> schemes;

	private ColourSchemeManager() {
		schemes = new Hashtable();
		createDefaults();
	}
	
	public ColourScheme createScheme(String name) {
		if(name==null) return null;
		ColourScheme c = schemes.get(name);
		if(c==null) {
			c = new ColourScheme(name);
			schemes.put(name, c);
		}
		return c;
	}
	
	public String[] list()
	{
		String[] r = new String[schemes.size()];
		
		Enumeration<String> e = schemes.keys();
		int i=0;
		while(e.hasMoreElements()) {
			r[i++] = e.nextElement();
		}
		return r;
	}
	
	public ColourScheme getScheme(String name) {
		if(name==null) return null;
		ColourScheme c = schemes.get(name);
		return c;
	}
	
	public ColourScheme removeScheme(String name) {
		if(name==null) return null;
		ColourScheme c = schemes.remove(name);
		return c;
	}
	
	private void createDefaults() {
		createScheme("default");
//		schemes.put(ColourScheme.DEFAULT.name, ColourScheme.DEFAULT);
		ColourScheme.DEFAULT = schemes.get("default");
		ColourScheme s;
		s = createScheme("blue");
		s.BACKGROUND_COLOUR = 0xdd000000 ;  // Format 0xaarrggbb  using Hex.
		s.HOVER_BACKGROUND_COLOUR = 0xFF000088;
		s.MDWN_BACKGROUND_COLOUR = 0 ;
		s.LINE_COLOUR = s.FILL_COLOUR = s.TEXT_COLOUR = 0xFF000088; //ColourScheme.col(0, 0, 128);
		s.HOVER_LINE_COLOUR = s.HOVER_FILL_COLOUR = s.HOVER_TEXT_COLOUR = 0xFF0000FF;
		s.MDWN_LINE_COLOUR = s.MDWN_FILL_COLOUR = s.MDWN_TEXT_COLOUR = 0xFF0000FF;

		s = createScheme("green");
		s.BACKGROUND_COLOUR = 0xdd000000 ;  // Format 0xaarrggbb  using Hex.
		s.HOVER_BACKGROUND_COLOUR = 0xFF008800;
		s.MDWN_BACKGROUND_COLOUR = 0 ;
		s.LINE_COLOUR = s.FILL_COLOUR = s.TEXT_COLOUR = 0xFF008800; //ColourScheme.col(0, 0, 128);
		s.HOVER_LINE_COLOUR = s.HOVER_FILL_COLOUR = s.HOVER_TEXT_COLOUR = 0xFF00FF00;
		s.MDWN_LINE_COLOUR = s.MDWN_FILL_COLOUR = s.MDWN_TEXT_COLOUR = 0xFF00FF00;

		s = createScheme("red");
		s.BACKGROUND_COLOUR = 0xdd000000 ;  // Format 0xaarrggbb  using Hex.
		s.HOVER_BACKGROUND_COLOUR = 0xFF880000;
		s.MDWN_BACKGROUND_COLOUR = 0 ;
		s.LINE_COLOUR = s.FILL_COLOUR = s.TEXT_COLOUR = 0xFF880000; //ColourScheme.col(0, 0, 128);
		s.HOVER_LINE_COLOUR = s.HOVER_FILL_COLOUR = s.HOVER_TEXT_COLOUR = 0xFFFF0000;
		s.MDWN_LINE_COLOUR = s.MDWN_FILL_COLOUR = s.MDWN_TEXT_COLOUR = 0xFFFF0000;
	}

}
