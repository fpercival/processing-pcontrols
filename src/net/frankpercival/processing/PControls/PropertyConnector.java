package net.frankpercival.processing.PControls;

public class PropertyConnector<T> {
	
	private FieldSetter s, d;
	String p, dp;
	
	private T val;

	public PropertyConnector(T inVal) {
		val = inVal;
	}
	
	protected void setProperty(Object object, String propertyName){
		if(object==null || propertyName==null) {
			s = null;
			return;
		}
		s = new FieldSetter(object);
		p = propertyName;
	}
	
	protected void setDest(Object object, String propertyName){
		if(object==null || propertyName==null) {
			d = null;
			return;
		}
		d = new FieldSetter(object);
		dp = propertyName;
	}
	
	public void pull()
	{
		if(!isSet() || d==null) return;
		val = get();
		d.set(dp, val);
	}
	
	public void push()
	{
		if(!isSet() || d==null) return;
		s.set(p, d.get(dp));
	}
	
	public T get(){
		if(!isSet()) return null;
		Object o = s.get(p);
		if(val.getClass()==Float.class) o = Float.parseFloat(""+o);
		else if(val.getClass()==Float.TYPE) o = Float.parseFloat(""+o);
		else if(val.getClass()==Integer.class) o = Integer.parseInt(""+o);
		else if(val.getClass()==Integer.TYPE) o = Integer.parseInt(""+o);
		else if(val.getClass()==Double.class) o = Double.parseDouble(""+o);
		else if(val.getClass()==Double.TYPE) o = Double.parseDouble(""+o);
		else if(val.getClass()==Boolean.class) o = Boolean.parseBoolean(""+o);
		else if(val.getClass()==Boolean.TYPE) o = Boolean.parseBoolean(""+o);
		return (T) o;
	}
	
	public void set(T newVal){
		val = newVal;
		if(!isSet()) return;
		s.set(p, val);
	}
	
	public boolean isSet(){
		return s!=null;
	}

}
