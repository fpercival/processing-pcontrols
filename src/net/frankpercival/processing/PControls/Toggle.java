package net.frankpercival.processing.PControls;
import processing.core.PApplet;
import processing.event.MouseEvent;


public class Toggle extends Button {
	
	boolean value;
	
	PropertyConnector<Boolean> link;
	boolean propertyReverse;

	public Toggle(ControlGroup group, String name, boolean val) {
		super(group, name);
		value = val;
		link = new PropertyConnector(value);
		link.setDest(this, "pval");
		propertyReverse=false;
	}

	public Toggle(ControlGroup group, String name) {
		this(group, name, true);
	}
	
	
	public Toggle setProperty(Object object, String property) {
		link.setProperty(object, property);
		return this;
	}
	
	public Toggle setProperty(String property) {
		link.setProperty(g.p, property);
		return this;
	}
	
	@Override
	public void mouseClick(MouseEvent e) {
		super.mouseClick(e);
		value=!value;
		link.push();
	}
	
	public void drawPrep(PApplet p){
		link.pull();
	}

	@Override
	public void draw(PApplet p) {
		float r = p.getFont().getSize2D()/2; //height - 10;
		float pd = border?(height-r)/2:height-r;
		float tx = x+pd+r;
		float ty = y+(height/2);
		
		drawEllipse(tx, ty, r*2, r*2, value==true);

		tx = tx+r+pd;
		ty = y+(height/2);
		p.textAlign(p.LEFT, p.CENTER);
		drawText(name, tx, ty);
	}

	public boolean value() {
		link.pull();
		return value;
	}

	public boolean getValue() {
		return value();
	}

	public Toggle setValue(boolean value) {
		this.value = value;
		link.push();
		return this;
	}

	public boolean getPval() {
		return value;
	}

	public void setPval(boolean value) {
		this.value = value;
	}

	public boolean isPropertyReverse() {
		return propertyReverse;
	}

	public void setPropertyReverse(boolean propertyReverse) {
		this.propertyReverse = propertyReverse;
	}

}
