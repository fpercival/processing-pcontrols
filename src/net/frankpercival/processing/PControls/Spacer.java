package net.frankpercival.processing.PControls;
import processing.core.PApplet;


public class Spacer extends Control {

	public Spacer(ControlGroup group, int distance) {
		this(group, distance, distance);
	}

	public Spacer(ControlGroup group, int w, int h) {
		super(group);
		width = w;
		height = h;
		init();
	}

	public void init() {
		border = false;
	}

	@Override
	public void draw(PApplet p) {
	}

}
