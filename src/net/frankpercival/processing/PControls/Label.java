package net.frankpercival.processing.PControls;
import processing.core.PApplet;


public class Label extends Control {
	
	String text;

	public Label(ControlGroup group, String text) {
		super(group);
		this.text = text;
		init();
	}

	public void init() {
		border = false;
		changeOnMouseDown = false;
		changeOnHover = false;
	}

	@Override
	public void draw(PApplet p) {
		p.textAlign(p.LEFT, p.CENTER);
		float tx = x;
		float ty = y+(height/2);
		drawText(text, tx, ty);
	}

	public String getText() {
		return text;
	}

	public Label setText(String text) {
		this.text = text;
		return this;
	}

}
