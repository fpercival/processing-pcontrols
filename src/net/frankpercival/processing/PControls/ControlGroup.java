package net.frankpercival.processing.PControls;
import java.lang.reflect.Constructor;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import processing.core.PApplet;
import processing.core.PConstants;
import processing.core.PMatrix3D;
import processing.event.*;


public class ControlGroup {
	
	public static int TOP = 1;
	public static int BOTTOM = 2;
	public static int LEFT = 3;
	public static int RIGHT = 4;
	public static int CENTER = 5;
	
	EvtHandler eh;
	
	PApplet p;
	
	float x1,y1,width,height,x2,y2, pad;
	
	float dx, dy, dw, dh, dx2, dy2;
	
	float minx, miny;
	float maxv;
	
	float defaultWidth;
	float defaultHeight;
	
	boolean msein;
	
	boolean active;
	
	int position;
	
	PMatrix3D originalMatrix;
	
	Vector<Control> controls;
	
	Control mouselock;
	Control msc;
	
	MouseEvent lastevent;
	
	ColourScheme scheme;
	
	PControls owner;
	
	ControlCreator creator;
	
	public ControlGroup(PApplet parent){
		this(parent, BOTTOM, null);
	}
	
	public ControlGroup(PApplet parent, int pos){
		this(parent, pos, null);
	}
	
	public ControlGroup(PApplet parent, int pos, PControls controls){
		init(parent, pos, controls);
	}
	
	protected void init(PApplet parent, int ps, PControls ctrls) {
		p=parent;
		position = ps;
		pad = 15;
		controls = new Vector();
		owner = ctrls;
		if(owner==null)
		{
			p.registerMethod("draw", this);
		}
		p.registerMethod("mouseEvent", this);
		p.registerMethod("keyEvent", this);
		if(p.isGL()) originalMatrix = p.getMatrix((PMatrix3D)null);
		eh = new EvtHandler();
		msein=false;
		active = true;
		scheme = ColourSchemeManager.MANAGER().getScheme("default");
		minx=0;
		miny=0;
		maxv=0;
		defaultWidth = 100;
		defaultHeight = 30;
		creator = new ControlCreator(this);
	}
	
	public ControlGroup onMouseEnter(String function, Object object){
		eh.addHandler("mousein", object, function, new Class[]{this.getClass()});
		return this;
	}
	
	public ControlGroup onMouseExit(String function, Object object){
		eh.addHandler("mouseout", object, function, new Class[]{this.getClass()});
		return this;
	}
	
	public void addControl(Control c) {
		controls.add(c);
		calcrect();
	}
	
	public void removeControl(Control c)
	{
		controls.remove(c);
		calcrect();
	}
	
	public Control control(String nme) {
		return findControl(nme);
	}
	
	protected Control findControl(String nme)
	{
		StringTokenizer st = new StringTokenizer(nme, ".");
		String cn = st.nextToken();
		Control c=null;
		Collection col;
		for(int i=0; i<controls.size() && c==null; i++)
		{
			c = controls.get(i);
			if(c.getSimpleName().equalsIgnoreCase(cn)) {
				while(c instanceof Collection && st.hasMoreTokens()) {
					col = (Collection)c;
					cn = st.nextToken();
					c = null;
					for(int j=0; j<col.cs.size() && c==null; j++)
					{
						c = col.cs.get(j);
						if(!c.getSimpleName().equalsIgnoreCase(cn))
						{
							c = null;
						}
					}
				}
			} else {
				c=null;
			}
		}
		if(!c.getName().equalsIgnoreCase(nme)) c=null;
		return c;
	}
	
	protected void prepdraw(){
		if(!p.isGL()) return;
		p.pushMatrix();
		p.hint(PConstants.DISABLE_DEPTH_TEST);
		// Load the identity matrix.
		p.resetMatrix();
		// Apply the original Processing transformation matrix.
		p.applyMatrix(originalMatrix);
	}
	
	public void draw(){
		if(active==false) return;
		prepdraw();

		drawControls();
		
		finishdraw();
	}
	
	void drawControls()
	{
		for(int i=0; i<controls.size(); i++){
			Control c = controls.elementAt(i);
			c.render(p);
		}
		for(int i=0; i<controls.size(); i++){
			Control c = controls.elementAt(i);
			c.draw2(p);
		}
	}
	
	public void finishdraw() {
		if(!p.isGL()) return;
		p.hint(PConstants.ENABLE_DEPTH_TEST);
		p.popMatrix();
	}
	
	public ControlGroup refresh() {
		calcrect();
		return this;
	}
	
	protected void calcrect(){
		float w=pad;
		float h=pad;
		float cw, ch;
		float rw = w;
		float rh = h;
		int rid=0;
		for(int i=0; i<controls.size(); i++){
			Control c = controls.elementAt(i);
			cw = c.getWidth();
			ch = c.getHeight();
			c.rid = rid;
			if(position==LEFT || position==RIGHT) 
			{
				if((rh+ch+pad)>getmax()) {
					rh = pad;
					rw += rowheight(rid++) + pad;
				}
			} else {
				if((rw+cw+pad)>getmax()) {
					rw = pad;
					rh += rowheight(rid++) + pad;
				}
			}
			c.x = rw;
			c.y = rh;
			if(position==LEFT || position==RIGHT)  {
				rh += ch+pad;
			} else {
				rw += cw+pad;
			}
			w = Math.max(w, rw);
			h = Math.max(h, rh);
		}
		if(position==LEFT || position==RIGHT) {
			w+=pad + rowheight(rid);
			h+=pad;
		} else {
			w+=pad;
			h+=pad + rowheight(rid);
		}
		if(controls.size()==0)
		{
			w = h = 0;
		}
		if(position==BOTTOM) {
			x1=0;
			y1=p.height-h ;
		}
		if(position==LEFT || position==TOP) {
			x1=0;
			y1=0;
		}
		if(position==RIGHT) {
			x1=p.width-w;
			y1=0;
		}
		if(position==CENTER) {
			x1 = (p.width-w)/2;
			y1 = (p.width-w)/2;
		}
		x1 += minx;
		y1 += miny;
		width = w;
		height = h;
		x2 = x1+width;
		y2 = y1+height;
		for(int i=0; i<controls.size(); i++){
			Control c = controls.elementAt(i);
			c.setX(x1+c.x);
			c.setY(y1+c.y);
			c.recalc();
		}
		if(owner!=null) owner.refresh();
	}
	
	protected float getmax()
	{
		float r=0;
		if(maxv!=0) r= maxv;
		else
		{
			if(position==LEFT || position==RIGHT) r = p.height - miny;
			else r = p.width - minx;
		}
		return r;
	}
	
	protected float rowheight(int rnum){
		float rh = 0;
		for(int i=0; i<controls.size(); i++){
			Control c = controls.elementAt(i);
			
			if(c.rid==rnum && !(c instanceof Spacer)) 
			{
				if(position==LEFT || position==RIGHT) 
				{
					rh = Math.max(rh, c.getWidth());
				} else {
					rh = Math.max(rh, c.getHeight());
				}
			}
		}
		return rh;
	}
	
	public boolean lockmouse(Control c) {
		if(mouselock==null) {
			mouselock = c;
			return true;
		}
		return false;
	}
	
	public void unlockmouse() {
		if(msein==true) 
		{
			eh.fireHanlder("mouseout", new Object[]{this});
		}
		if(mouselock!=null && !mouselock.eventInControl(lastevent)) {
			mouselock.mouseExit(lastevent);
		}
		msein=false;
		mouselock = null;
	}
	
	public void mouseEvent(MouseEvent e){
		if(active==false) return;

		int mx = e.getX();
		int my = e.getY();
		if(mouselock!=null) {
			mouselock.handleMouseEvent(e);
			return;
		}
		if(msein==false && mx>=x1 && mx<=x2 && my>=y1 && my<=y2) {
			msein=true;
			eh.fireHanlder("mousein", new Object[]{this});
		} else if(msein==true && (mx<x1 || mx>x2 || my<y1 || my>y2))
		{
			msein=false;
			eh.fireHanlder("mouseout", new Object[]{this});
		}
		Control lmsc=null;
		for(int i=0; i<controls.size(); i++){
			Control c = controls.elementAt(i);
			if(c.eventInControl(e)){
				lmsc=c;
				c.handleMouseEvent(e);
			};
		}
		setmsc(lmsc, e);
		lastevent = e;
	}
	
	protected void setmsc(Control c, MouseEvent e){
		if(c==null && msc==null) return;
		if(c!=null && msc!=null && c==msc) return;
		if(c!=null) {
			if(msc!=null && msc!=c) {
//System.out.println("CG msc change: "+msc+" -> "+c);
				msc.handleMouseExit(e);
				c.handleMouseEnter(e);
			} else {
//System.out.println("CG mse enter: "+c);
				c.handleMouseEnter(e);
			}
		} else {
			if(msc!=null) msc.handleMouseExit(e);
//System.out.println("CG mse exit: "+msc);		
		}
		msc=c;
	}
	
	public void keyEvent(KeyEvent e){
		if(active==false) return;
		for(int i=0; i<controls.size(); i++){
			Control c = controls.elementAt(i);
			c.keyEvent(e);
		}
	}
	
	public void testfill(int nm) {
		testfill("Test", nm);
	}
	
	public void testfill(String txt, int nm) {
		for(int i=0; i<nm; i++) {
			create("Toggle", txt+(i+1)).setDimensions( (float)(80+(Math.random()*40)), (float)(20+(Math.random()*40)));
		}
	}

	public boolean isActive() {
		return active;
	}

	public ControlGroup setActive(boolean active) {
		this.active = active;
		if(active==true) refresh();
		return this;
	}

	public float getDefaultWidth() {
		return defaultWidth;
	}

	public ControlGroup setDefaultWidth(float defaultWidth) {
		this.defaultWidth = defaultWidth;
		return this;
	}

	public float getDefaultHeight() {
		return defaultHeight;
	}

	public ControlGroup setDefaultHeight(float defaultHeight) {
		this.defaultHeight = defaultHeight;
		return this;
	}

	public Control create(String name, Object... objs) {
		Control cn = creator.create(name, objs);
		if(cn!=null) addControl(cn);
		return cn;
	}
	
	protected ColourScheme scheme() {
		if(scheme==null && owner!=null) {
			return owner.scheme;
		}
		return scheme;
	}
	
	public String getColourScheme(String sname) {
		return scheme().name;
	}
	
	public ControlGroup setColourScheme(String sname) {
		ColourScheme s= ColourSchemeManager.MANAGER().getScheme(sname);
//System.out.println("Scheme '"+sname+"' = "+s);
		if(s!=null) scheme=s;
		return this;
	}
	
	

}
