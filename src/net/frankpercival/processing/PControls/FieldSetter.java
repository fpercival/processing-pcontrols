package net.frankpercival.processing.PControls;
import java.lang.reflect.Method;
import java.util.Hashtable;


public class FieldSetter {
	
	Object obj;
	
	Hashtable<String, Method> meths;

	public FieldSetter() {
		init(this);
	}

	public FieldSetter(Object object) {
		init(object);
	}
	
	public void init(Object o){
		obj = o;
		meths = new Hashtable();
	}
	
	protected Method getMeth(String fn, Object ...objs){
		Method m = meths.get(fn);
		if(m==null) {
			m = Helper.findMethod(obj, fn, objs);
			if(m!=null) {
				meths.put(fn, m);
			}
		}
		return m;
	}
	
	public FieldSetter set(String fld, Object ...objs)
	{
		String fn = "set"+cap(fld);
		try
		{
			Method m = getMeth(fn, objs);
			if(m!=null) {
				m.invoke(obj, objs);
			} else {
				System.err.println("NoSuchMethod "+obj.getClass().getName()+"."+fn);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return this;
	}
	
	public Object get(String fld, Object ...objs)
	{
		String fn = "get"+cap(fld);
		try
		{
			Method m = getMeth(fn, objs);
//			Method m = Helper.findMethod(obj, fn, null);
			if(m!=null) {
				Object o = m.invoke(obj, objs);
				return o;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	protected String cap(String s) {
		return s.substring(0,1).toUpperCase() + s.substring(1);
	}

}
