package net.frankpercival.processing.PControls;

public class ColourScheme {
	
	public static ColourScheme DEFAULT = new ColourScheme("default");
	
	String name;
	
	public int BACKGROUND_COLOUR = 0xdd000000 ;  // Format 0xaarrggbb  using Hex.
	public int HOVER_BACKGROUND_COLOUR = 128 ;
	public int MDWN_BACKGROUND_COLOUR = 0 ;

	public int LINE_COLOUR = 128;
	public int FILL_COLOUR = 128;
	public int TEXT_COLOUR = 128;

	public int HOVER_LINE_COLOUR = 255;
	public int HOVER_FILL_COLOUR = 255;
	public int HOVER_TEXT_COLOUR = 255;

	public int MDWN_LINE_COLOUR = 255;
	public int MDWN_FILL_COLOUR = 255;
	public int MDWN_TEXT_COLOUR = 255;

	public int CORNER_RADIUS = 8;
	
	public boolean FILL_AS_DEFAULT = true; 

	public ColourScheme(String nme) {
		this.name = nme;
	}
	
	public static String[] list(){
		return ColourSchemeManager.MANAGER().list();
	}
	
	public static ColourScheme get(String name){
		return ColourSchemeManager.MANAGER().getScheme(name);
	}
	
	public static ColourScheme create(String name){
		return ColourSchemeManager.MANAGER().createScheme(name);
	}
	
	public static int col(int r, int g, int b){
		return col(r, g, b, 255);
	}
	
	public static int col(int r, int g, int b, int a){
		int rt = 0;
/*
		rt += (a << 24) & 0xFF;
		rt += (r << 16) & 0xFF;
		rt += (b << 8) & 0xFF;
		rt += b & 0xFF;
*/
		rt += (a << 24);
		rt += (r << 16);
		rt += (g << 8);
		rt += b;
		return rt;
	}

}
